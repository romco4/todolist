import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { SpinnerOverlayService } from './spinner-overlay/spinner-overlay.service';

@Injectable()
export class BasicInterceptor implements HttpInterceptor {

  constructor (private spinner: SpinnerOverlayService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.spinner.show();
    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        this.spinner.hide();
        return event;
      })
    );
  }
}
