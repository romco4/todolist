import { Component, OnDestroy, OnInit } from '@angular/core';
import { ServerService } from '../server.service';
import { Subscription } from 'rxjs';
import { Item } from '../item/item';
import {MatCardModule} from '@angular/material/card';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { NewItemDialogComponent } from '../new-item-dialog/new-item-dialog.component';
import { ItemComponent } from '../item/item.component';
import { ItemFilterPipe } from '../item-filter.pipe';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [MatDialogModule, MatCardModule, ItemComponent, ItemFilterPipe, FormsModule, ReactiveFormsModule],
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss'
})
export class ListComponent implements OnInit, OnDestroy {

  items: Array<Item> = [];
  filteredItems: Array<Item> = [];
  types = ['todo', 'resolved'];
  subs = new Subscription();

  filter = new FormGroup({
    title: new FormControl(''),
    text: new FormControl('')
  })

  constructor(private server: ServerService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.getItems();

    this.subs.add(this.filter.valueChanges.subscribe(res => {
      this.filteredItems = structuredClone(this.items);
      this.filteredItems = this.filteredItems.filter(
        item =>
        item.title.toLowerCase().includes(res.title?.toLowerCase() ?? '') &&
        item.text.toLowerCase().includes(res.text?.toLowerCase() ?? '')
      );
    }));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  getItems() {
    this.subs.add(this.server.getItems().subscribe((res: any) => {
      this.items = this.filteredItems = res;

    }));
  }

  newItem() {
    this.subs.add(this.dialog.open(NewItemDialogComponent, {
      width: '50%'
    }).afterClosed().subscribe((res: any) => {
      if (res) {
        this.create(res);
      }
    }));
  }

  create(item: Item) {
    this.subs.add(this.server.createItem(item).subscribe(res => {
      console.log('createNew', res);
      this.getItems();
    }));
  }

  resolveItem(item: Item) {
    item.state = 'resolved';
    this.update(item);
  }

  update(item: Item) {
    this.subs.add(this.server.updateItem(item).subscribe(res => {
      console.log('update', res);
      this.getItems();
    }));
  }

  remove(id: any) {
    this.subs.add(this.server.deleteItem(id).subscribe(res => {
      console.log('remove', res);
      this.getItems();
    }));
  }
}
