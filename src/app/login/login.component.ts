import { Component } from '@angular/core';
import { ServerService } from '../server.service';
import { FormGroup, FormControl, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {

  login = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  error = null;

  constructor(private server: ServerService) {
    // this.server.logout();
  }

  submitLogin() {
    this.server.login(this.login.get('email')?.value ?? undefined, this.login.get('password')?.value ?? undefined).then(err => {
      this.error = err;
      setTimeout(() => {
        this.error = null;
      }, 5000);
    });
  }

}
