import { Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { AuthGuard } from './guard/auth.guard';
import { LoginComponent } from './login/login.component';

export const routes: Routes = [
  { path: 'login', component: LoginComponent },


  { path: '', component: ListComponent, canActivate: [AuthGuard] },
  { path: 'home', component: ListComponent, canActivate: [AuthGuard] },
  { path: '**', component: ListComponent, canActivate: [AuthGuard] },

];
