import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Auth, signInWithEmailAndPassword } from '@angular/fire/auth'
import { Router } from '@angular/router';
import { Item } from './item/item';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  constructor(private http: HttpClient, private auth: Auth, private router: Router) {}

  async init() {
    return new Promise((resolve: any) => {
      setTimeout(() => {
        const user = this.auth.currentUser;
        if (user) {
          localStorage.setItem('user', JSON.stringify(user));
        } else {
          localStorage.setItem('user', JSON.stringify(null));
        }
        resolve();
      }, 2000);
    });
  }


  /**
   * CRUD services for items
   */
  getItems() {
    return this.http.get('https://6602a91b9d7276a75553e0e3.mockapi.io/api/v1/item');
  }

  createItem(item: Item) {
    const params = item;
    return this.http.post('https://6602a91b9d7276a75553e0e3.mockapi.io/api/v1/item', params);
  }

  updateItem(item: Item) {
    const params = item;
    const id = item.id;
    return this.http.put('https://6602a91b9d7276a75553e0e3.mockapi.io/api/v1/item/' + id, params);
  }

  deleteItem(id: string) {
    return this.http.delete('https://6602a91b9d7276a75553e0e3.mockapi.io/api/v1/item/' + id);
  }


  /**
   * Auth services
   */
  login(email = 'ferko@hurka.com', password = 'password') {
    return signInWithEmailAndPassword(this.auth, email, password)
      .then((result) => {
        localStorage.setItem('user', JSON.stringify(result.user));
        this.router.navigate(['/']);
      }).catch(error => {
        return error;
      });
  }

  logout() {
    this.auth.signOut().then(() => {
      localStorage.setItem('user', JSON.stringify(null));
      this.router.navigate(['/login']);
    }).catch(() => {
      console.error('Something went wrong');
    });
  }

  // Returns true when user is looged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user') ?? 'null');
    // this.logout()
    return (user !== null) ? true : false;
  }
}
