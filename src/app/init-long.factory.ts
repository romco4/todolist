import { ServerService } from "./server.service";

export function initLongRunningFactory(
  server: ServerService
) {
  return async () => {
    // console.log('initLongRunningFactory - started');
    await server.init();
    // console.log('initLongRunningFactory - completed');
  };
}
