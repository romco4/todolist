import { Injectable } from '@angular/core';
import { UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ServerService } from '../server.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {

  constructor(
    public server: ServerService,
    public router: Router,
  ) {}

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.server.isLoggedIn) {
        this.router.navigate(['/login']);
      }
      return true;
  }

}
