import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { SpinnerOverlayComponent } from '../spinner-overlay/spinner-overlay.component';

@Injectable({
  providedIn: 'root'
})
export class SpinnerOverlayService {

  private overlayRef: OverlayRef | undefined;
  private overlayRequestsCount = 0;

  constructor(private overlay: Overlay) {}

  public show(message = '') {
    this.overlayRequestsCount++;

    if (this.overlayRef === undefined) {
      this.overlayRef = this.overlay.create();

      const spinnerOverlayPortal = new ComponentPortal(SpinnerOverlayComponent);
      const component = this.overlayRef.attach(spinnerOverlayPortal);
    }

  }

  public hide() {
    this.overlayRequestsCount--;
    setTimeout(() => {
      if (this.overlayRef !== null && this.overlayRequestsCount <= 0) {
        this.overlayRef?.detach();
        this.overlayRef = undefined;
        this.overlayRequestsCount = 0;
      }
    }, 700);

  }
}
