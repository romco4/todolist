import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import { Item } from './item';

@Component({
  selector: 'app-item',
  standalone: true,
  imports: [MatCardModule, CommonModule],
  templateUrl: './item.component.html',
  styleUrl: './item.component.scss'
})
export class ItemComponent {

  @Input() item: any;
  @Output() remove = new EventEmitter<string>();
  @Output() resolve = new EventEmitter<Item>();

}
