export interface Item {
  id: string;
  state: string;
  createdAt: Date;
  title: string;
  text: string;
  deadline: Date;
}
