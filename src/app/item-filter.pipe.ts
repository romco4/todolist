import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'itemFilter',
  standalone: true
})
export class ItemFilterPipe implements PipeTransform {

  transform(items: any[], filter: any): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item => item.state === filter.state);
  }

}
